Piscopolitan
Author: Roberto Cuadra
Recipe type: Bebidas
Prep time:  5 mins Total time:  5 mins
Serves: 1

Ingredients

2 oz. de Pisco (recomendamos Quebranta)
1¼ oz. de jugo de limón verde
1¼ oz. de jugo de cranberry
¾ oz. de Cointreau
5 cubos de hielo
2 cereza Marraschino
1 rodajita de limón

Instructions
En una coctelera combina el Pisco, jugo de limón, jugo de cranberry, Cointreau y hielo.
Agita bien y sirve sin el hielo en una copa previamente enfriada.

Decora con una cereza y una rodaja de limón.
